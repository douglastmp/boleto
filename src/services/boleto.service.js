const datefns = require("date-fns");

/**
 * Verifica se uma linha digitãvel é válida
 * @param {string} linhaDigitavel
 * @returns {Boolean}
 */
const validarBoleto = async (linhaDigitavel) => {
  return linhaDigitavel.length === 47 || linhaDigitavel.length === 48;
};

/**
 * Verifica se os digitos verificadores de uma linha digitãvel é válida
 * @param {string} linhaDigitavel
 * @returns {Boolean}
 */
const validarDigitoBoleto = async (linhaDigitavel) => {
  let resultado;
  if (linhaDigitavel.substr(0, 1) !== 8) {
    const bloco1 =
      linhaDigitavel.substr(0, 9) + calculaMod10(linhaDigitavel.substr(0, 9));
    const bloco2 =
      linhaDigitavel.substr(10, 10) +
      calculaMod10(linhaDigitavel.substr(10, 10));
    const bloco3 =
      linhaDigitavel.substr(21, 10) +
      calculaMod10(linhaDigitavel.substr(21, 10));
    const bloco4 = linhaDigitavel.substr(32, 1);
    const bloco5 = linhaDigitavel.substr(33);

    resultado = (bloco1 + bloco2 + bloco3 + bloco4 + bloco5).toString();
  } else {
    let bloco1;
    let bloco2;
    let bloco3;
    let bloco4;

    if (
      linhaDigitavel.substr(2, 1) === "6" ||
      linhaDigitavel.substr(2, 1) === "7"
    ) {
      bloco1 =
        linhaDigitavel.substr(0, 11) +
        calculaMod10(linhaDigitavel.substr(0, 11));
      bloco2 =
        linhaDigitavel.substr(12, 11) +
        calculaMod10(linhaDigitavel.substr(12, 11));
      bloco3 =
        linhaDigitavel.substr(24, 11) +
        calculaMod10(linhaDigitavel.substr(24, 11));
      bloco4 =
        linhaDigitavel.substr(36, 11) +
        calculaMod10(linhaDigitavel.substr(36, 11));
    } else if (
      linhaDigitavel.substr(2, 1) === "8" ||
      linhaDigitavel.substr(2, 1) === "9"
    ) {
      bloco1 = linhaDigitavel.substr(0, 11);
      bloco2 = linhaDigitavel.substr(12, 11);
      bloco3 = linhaDigitavel.substr(24, 11);
      bloco4 = linhaDigitavel.substr(36, 11);

      let dv1 = parseInt(linhaDigitavel.substr(11, 1));
      let dv2 = parseInt(linhaDigitavel.substr(23, 1));
      let dv3 = parseInt(linhaDigitavel.substr(35, 1));
      let dv4 = parseInt(linhaDigitavel.substr(47, 1));

      let valid =
        calculaMod11(bloco1) == dv1 &&
        calculaMod11(bloco2) == dv2 &&
        calculaMod11(bloco3) == dv3 &&
        calculaMod11(bloco4) == dv4;

      return valid;
    }

    resultado = bloco1 + bloco2 + bloco3 + bloco4;
  }
  return linhaDigitavel === resultado;
};

/**
 * Retorna o código de barras de uma linhaDigitável
 * @param {string} linhaDigitav1997el
 * @returns {string}
 */
const checarCodigoBarras = async (linhaDigitavel) => {
  if (linhaDigitavel.substr(0, 1) !== 8) {
    return (
      linhaDigitavel.substr(0, 4) +
      linhaDigitavel.substr(32, 1) +
      linhaDigitavel.substr(33, 14) +
      linhaDigitavel.substr(4, 5) +
      linhaDigitavel.substr(10, 10) +
      linhaDigitavel.substr(21, 10)
    );
  } else {
    const codigo = linhaDigitavel.split("");
    codigo.splice(11, 1);
    codigo.splice(22, 1);
    codigo.splice(33, 1);
    codigo.splice(44, 1);
    return codigo.join("");
  }
};

/**
 * Retorna o preço de um código de barras através de uma linhaDigitável
 * @param {string} linhaDigitavel
 * @returns {string}
 */
const checarPreco = async (linhaDigitavel) => {
  if (linhaDigitavel.substr(0, 1) !== 8) {
    let valorBoleto = linhaDigitavel.substr(37);
    let valorFinal = valorBoleto.substr(0, 8) + "." + valorBoleto.substr(8, 2);

    let char = valorFinal.substr(1, 1);
    while (char === "0") {
      valorFinal = substringReplace(valorFinal, "", 0, 1);
      char = valorFinal.substr(0, 1);
    }
    return valorFinal;
  } else {
    let valorBoleto = linhaDigitavel.substr(4, 14);
    valorBoleto = linhaDigitavel.split("");
    valorBoleto.splice(11, 1);
    valorBoleto = valorBoleto.join("");
    valorBoleto = valorBoleto.substr(4, 11);
    return valorBoleto;
  }
};

/**
 * Retorna a data de expiração de um código de barras através de uma linhaDigitável
 * @param {string} linhaDigitavel
 * @returns {string}
 */
const checarDataExpiracao = async (linhaDigitavel) => {
  if (linhaDigitavel.substr(0, 1) === 8) {
    return 0;
  } else {
    const expirationDate = datefns.addDays(
      new Date(1997, 09, 07),
      linhaDigitavel.substr(33, 4)
    );
    return datefns.format(new Date(expirationDate), "dd/MM/yyyy");
  }
};

const substringReplace = (str, repl, inicio, tamanho) => {
  if (inicio < 0) {
    inicio = inicio + str.length;
  }

  tamanho = tamanho !== undefined ? tamanho : str.length;
  if (tamanho < 0) {
    tamanho = tamanho + str.length - inicio;
  }

  return [
    str.slice(0, inicio),
    repl.substr(0, tamanho),
    repl.slice(tamanho),
    str.slice(inicio + tamanho),
  ].join("");
};

/**
 * Calcula o dígito verificador de uma numeração a partir do módulo 10
 * @param {string} numero Numeração
 * @return {string} soma
 */
const calculaMod10 = (numero) => {
  numero = numero.replace(/\D/g, "");
  var i;
  var mult = 2;
  var soma = 0;
  var s = "";

  for (i = numero.length - 1; i >= 0; i--) {
    s = mult * parseInt(numero.charAt(i)) + s;
    if (--mult < 1) {
      mult = 2;
    }
  }
  for (i = 0; i < s.length; i++) {
    soma = soma + parseInt(s.charAt(i));
  }
  soma = soma % 10;
  if (soma != 0) {
    soma = 10 - soma;
  }
  return soma;
};

/**
 * Calcula o dígito verificador de uma numeração a partir do módulo 11
 * @param {string} x Numeração
 * @return {string} digito
 */
const calculaMod11 = (numero) => {
  let sequencia = [4, 3, 2, 9, 8, 7, 6, 5];
  let digit = 0;
  let j = 0;
  let DAC = 0;

  //FEBRABAN https://cmsportal.febraban.org.br/Arquivos/documentos/PDF/Layout%20-%20C%C3%B3digo%20de%20Barras%20-%20Vers%C3%A3o%205%20-%2001_08_2016.pdf
  for (var i = 0; i < x.length; i++) {
    let mult = sequencia[j];
    j++;
    j %= sequencia.length;
    digit += mult * parseInt(x.charAt(i));
  }

  DAC = digit % 11;

  if (DAC == 0 || DAC == 1) return 0;
  if (DAC == 10) return 1;

  return 11 - DAC;
};

module.exports = {
  validarBoleto,
  validarDigitoBoleto,
  checarCodigoBarras,
  checarPreco,
  checarDataExpiracao,
};
