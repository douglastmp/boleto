const Joi = require('joi');

const retornaInformacoesBoleto = {
  params: Joi.object().keys({
    linhaDigitavel: Joi.number().unsafe().integer().required(),
  }),
}

module.exports = {
  retornaInformacoesBoleto,
};