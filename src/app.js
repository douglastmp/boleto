const express = require('express');
const cors = require('cors');
const routes = require('./routes/v1')

const app = express()

app.use(cors())

app.use('/v1', routes);

module.exports = app;