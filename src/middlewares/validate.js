const Joi = require("joi");
const _ = require("lodash");

const validate = (schema) => (req, res, next) => {
  const validSchema = _.pick(schema, ["params", "query", "body"]);
  const object = _.pick(req, Object.keys(validSchema));
  const { error } = Joi.compile(validSchema)
    .prefs({ errors: { label: "key" }, abortEarly: false })
    .validate(object);

  if (error) {
    const errorMessage = error.details
      .map((details) => details.message)
      .join(", ");
    res.status(400).json({ errorMessage });
  } else {
    return next();
  }
};

module.exports = validate;
