const express = require("express");
const { boletoController } = require("../../controllers");
const {
  retornaInformacoesBoleto,
} = require("../../validations/boleto.validation");
const validate = require("../../middlewares/validate");

const router = express.Router();

router.get(
  "/boleto/:linhaDigitavel",
  validate(retornaInformacoesBoleto),
  boletoController.retornaInformacoesBoleto
);

module.exports = router;
