const { boletoService } = require("../services");

const retornaInformacoesBoleto = async (req, res) => {
  const { linhaDigitavel } = req.params;
  const boletoValido = await boletoService.validarBoleto(linhaDigitavel);
  const digitoValido = await boletoService.validarDigitoBoleto(linhaDigitavel);
  if (boletoValido && digitoValido) {
    const barCode = await boletoService.checarCodigoBarras(linhaDigitavel);
    const amount = await boletoService.checarPreco(linhaDigitavel);
    const expirationDate = await boletoService.checarDataExpiracao(
      linhaDigitavel
    );
    res.send({ barCode, amount, expirationDate });
  } else {
    res.status(400).json({ message: "Linha digitável inválida" });
  }
};

module.exports = {
  retornaInformacoesBoleto,
};
