const chai = require("chai");
const sinon = require("sinon");
const chaiAsPromised = require("chai-as-promised");
const boletoService = require("../src/services/boleto.service");

chai.use(chaiAsPromised);
const {expect} = chai;

const sandBox = sinon.createSandbox();

describe("boleto-v1", () => {
  describe("boleto controller", () => {
    it("deve retornar verdadeiro se a linha digitavel for valida", async () => {
      const boletoValido = await boletoService.validarBoleto('21290001192110001210904475617405975870000002000');
      expect(boletoValido).to.be.true
    });

    it("deve retornar falso se a linha digitavel não for valida", async () => {
        const boletoValido = await boletoService.validarBoleto('teste');
        expect(boletoValido).to.be.false
    });

    it("deve retornar falso se a linha digitavel não for valida", async () => {
        const boletoValido = await boletoService.validarBoleto('2129000119211000121090');
        expect(boletoValido).to.be.false
    });

    it("deve retornar o código de barras se a linha digitavel for valida", async () => {
        const codBarras = await boletoService.checarCodigoBarras('21290001192110001210904475617405975870000002000');
        expect(codBarras).to.be.equals('21299758700000020000001121100012100447561740')
    });

    it("deve retornar o preço do título se a linha digitavel for valida", async () => {
        const preco = await boletoService.checarPreco('21290001192110001210904475617405975870000002000');
        expect(preco).to.be.equals('20.00')
    });

    it("deve retornar a data de vencimento se a linha digitavel for valida", async () => {
        const codBarras = await boletoService.checarDataExpiracao('21290001192110001210904475617405975870000002000');
        expect(codBarras).to.be.equals('16/07/2018')
    });
  });
});
